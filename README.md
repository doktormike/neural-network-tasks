# Neural Network Tasks

[![pipeline status](https://gitlab.com/doktormike/neural-network-tasks/badges/main/pipeline.svg)](https://gitlab.com/doktormike/neural-network-tasks/-/commits/main)
[![coverage report](https://gitlab.com/doktormike/neural-network-tasks/badges/main/coverage.svg)](https://gitlab.com/doktormike/neural-network-tasks/-/commits/main)
[![Latest Release](https://gitlab.com/doktormike/neural-network-tasks/-/badges/release.svg)](https://gitlab.com/doktormike/neural-network-tasks/-/releases)
[![Code style black](https://img.shields.io/badge/code%20style-black-black)](https://black.readthedocs.io/en/stable/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-%23FE5196?logo=conventionalcommits&logoColor=white)](https://conventionalcommits.org)

## Getting started

This is a collection of tasks that can be solved with neural networks. None of
these tasks are solving anything useful on their own but, they are meant as
inspiration of what you could build of them.

## Tasks 

- Text to Speech (tts)
- Speech to Text (stt)
- Text to Text (ttt)


