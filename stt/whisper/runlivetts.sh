#!/bin/bash

# This listens to device 1 and uses the base model to run stt
./stream -m models/ggml-base.en.bin -c 1     


# Run with default arguments and small model
./command -m ./models/ggml-base.en.bin -t 8

# On Raspberry Pi, use tiny or base models + "-ac 768" for better performance
./command -m ./models/ggml-tiny.en.bin -ac 768 -t 3 -c 0

# Run in guided mode, the list of allowed commands is in commands.txt
./command -m ./models/ggml-base.en.bin -cmd ./examples/command/commands.txt
