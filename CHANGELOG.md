# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/doktormike/neural-network-tasks/compare/v1.0.0...v1.0.1) (2023-12-04)


### Bug Fixes

* small change in text. ([0543899](https://gitlab.com/doktormike/neural-network-tasks/commit/0543899b3b7fc93b1c78d49909044ec8d83286a6))


### Documentation

* added badges to the readme. ([dfd0559](https://gitlab.com/doktormike/neural-network-tasks/commit/dfd05590b627947a4072a67d7315fc392c411593))
* more description. ([2f107dd](https://gitlab.com/doktormike/neural-network-tasks/commit/2f107dd4185d93bf89cf4f2e73ebf46351327fc5))

## 1.0.0 (2023-12-03)


### Features

* added a text-to-speech example which is really great. ([7c219ba](https://gitlab.com/doktormike/neural-network-tasks/commit/7c219ba0a9943cfae58abfe5cf37872b4011c6d3))
* added citation and requirements. ([1ef6378](https://gitlab.com/doktormike/neural-network-tasks/commit/1ef6378470d5268c2b06436a231720b6dcf22c45))


### Documentation

* added a readme. ([b033b67](https://gitlab.com/doktormike/neural-network-tasks/commit/b033b6714516bfb460d06b9491ee9899d1566b3b))
