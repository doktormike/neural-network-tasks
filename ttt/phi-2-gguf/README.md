
### Download Phi-2 gguf file: 

```bash
wget -c https://huggingface.co/TheBloke/phi-2-GGUF/blob/main/phi-2.Q6_K.gguf
```

### Clone the llama.cpp repo and build it 

```bash
git clone git@github.com:ggerganov/llama.cpp.git 
cd llama.cpp
make
```

### Start the server

```bash
# ./server -m models/7B/ggml-model.gguf -c 2048
# ./server -m /path/to/mistral-7b-openorca.Q4_K_M.gguf
./server -m ../phi-2.Q6_K.gguf
```

### Test the server

```bash 
curl --request POST \
    --url http://localhost:8080/completion \
    --header "Content-Type: application/json" \
    --data '{"prompt": "Building a website can be done in 10 simple steps:","n_predict": 128}'
```

