# Text similarity task

## Introduction

Computing the similarity between a pair of sentences. Typically they can be
Question <-> Document relation but could also just be two separate sentences
that you want to check the semantic similarity of.
