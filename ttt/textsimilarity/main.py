import torch
from transformers import AutoModelForSequenceClassification, AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained("BAAI/bge-reranker-large")
model = AutoModelForSequenceClassification.from_pretrained("BAAI/bge-reranker-large")
model.eval()

# The pairs are sort of questions <-> document pairing but can also just be used to see semantic
# score for two independent sentences.
pairs = [
    [
        "My girlfriend is being a bitch and I fucking hate it.",
        "My partner is treating me badly and it leaves me feeling a bit hurt and angry inside.",
    ],
    [
        "The car is awful and I don't want to buy it.",
        "The car is beautiful and I would like to buy it.",
    ],
    ["what is a panda?", "hi"],
    ["what is a panda?", "It's a bear like mammal predominantly existing in China."],
    [
        "what is panda?",
        "The giant panda (Ailuropoda melanoleuca), sometimes called a panda bear or simply panda, is a bear species endemic to China.",
    ],
    ["Han er en doven mand!", "Han laver sgu ikke meget!"],
]
with torch.no_grad():
    inputs = tokenizer(
        pairs, padding=True, truncation=True, return_tensors="pt", max_length=512
    )
    scores = (
        model(**inputs, return_dict=True)
        .logits.view(
            -1,
        )
        .float()
    )
    print(scores)
