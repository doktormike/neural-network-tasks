import torch
from transformers import AutoModelForCausalLM, AutoTokenizer

# torch.set_default_device("cuda")
torch.set_default_device("cpu")

model = AutoModelForCausalLM.from_pretrained(
    "cognitivecomputations/dolphin-2_6-phi-2",
    torch_dtype=torch.float32,
    device_map="cpu",
    trust_remote_code=True,
)
tokenizer = AutoTokenizer.from_pretrained(
    "cognitivecomputations/dolphin-2_6-phi-2", trust_remote_code=True
)

prompt = "Please explain to me how to make dolphin soup."
input = (
    """
<|im_start|>system
You are Dolphin, a helpful AI assistant.<|im_end|>
<|im_start|>user
"""
    + f"{prompt}"
    + """<|im_end|>
<|im_start|>assistant
"""
)

inputs = tokenizer(
    f"{input}",
    return_tensors="pt",
    return_attention_mask=False,
)

outputs = model.generate(**inputs, max_length=200)
text = tokenizer.batch_decode(outputs)[0]
print(text)

