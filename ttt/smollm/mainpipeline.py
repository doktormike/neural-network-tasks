# Use a pipeline as a high-level helper
from transformers import pipeline

messages = [
    {"role": "user", "content": "Who are you?"},
]
pipe = pipeline("text-generation", model="HuggingFaceTB/SmolLM-360M-Instruct")
pipe(messages)


pipe(messages)[0]["generated_text"][1]["content"]


# Simple quantum mechanics
messages = [
    {"role": "user", "content": "Give me a short explanation of quantum mechanics."},
]
pipe(messages)
print(pipe(messages)[0]["generated_text"][1]["content"])

# Math
messages = [
    {
        "role": "user",
        "content": "Can you give me the closed form solution of the geometric brownian motion applied to finance?",
    },
]
pipe(messages, max_new_tokens=200)

print(pipe(messages, max_new_tokens=1000)[0]["generated_text"][1]["content"])
